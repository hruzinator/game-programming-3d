﻿using UnityEngine;
using System.Collections;

public class EndPlaneScript : MonoBehaviour {

	public Texture2D buttonTexture;
	public GameObject instructGUI;
	
	private Vector2 screenCenter;
	private Vector2 menuDims = new Vector2(300f, 170f);

	private bool render = false;

	void Start(){
		screenCenter = new Vector2 (Screen.width / 2, Screen.height / 2);
	}

	void OnTriggerEnter(Collider col){
		if (col.tag == "Player") {
			TKController.timerController (false);
			render = true;
		}
	}

	void OnGUI () {
		if(render){
			float xOffset = menuDims.x/2;
			float yOffset = menuDims.y/2;
			GUI.Box(new Rect(screenCenter.x-xOffset,screenCenter.y-yOffset,menuDims.x,menuDims.y), "");
		
			// Make the first button. If it is pressed, Application.Loadlevel (1) will be executed
			if(GUI.Button(new Rect(screenCenter.x-xOffset,screenCenter.y-yOffset + 30,menuDims.x, 30), "Play Again")) {
				Application.LoadLevel(1);
			}
		
			// Make a button for outlining game information
			if(GUI.Button(new Rect(screenCenter.x-xOffset,screenCenter.y-yOffset + 75,menuDims.x, 30), "Instructions")) {
				gameObject.SetActive(false);
				instructGUI.SetActive(true);
			}
		
			// Make an exit button
			if(GUI.Button(new Rect(screenCenter.x-xOffset,screenCenter.y-yOffset + 120,menuDims.x, 30), "Quit")) {
				Application.Quit();
			}
		}
	}
}
