﻿using UnityEngine;
using System.Collections;

public class MenuKeyController : MonoBehaviour {

	Camera[] cameras;

	private int currCameraIdx = 0;

	void Start(){
		cameras = Camera.allCameras;
	}
	
	void LateUpdate () {
		if (Input.GetKeyDown (KeyCode.F1)) {
			currCameraIdx = (currCameraIdx+1)%cameras.Length;
			for (int i=0 ;i< cameras.Length ; i++) {
				// Activate the selected camera
				if (i == currCameraIdx){
					cameras[i].camera.active = true;
					// Deactivate all other cameras
				}else{
					cameras[i].camera.active = false;
				}
			}
		}

		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit();
		}
	}
}
