﻿using UnityEngine;
using System.Collections;

//The Timekeeper script for when the charachter moves down the mountain
public class TKController : MonoBehaviour {
	public GUIText hud;

	public static float timer = 0f;

	private static bool clockRunning = false;

	void Start(){
		timer = 0f;
	}

	// Update is called once per frame
	void Update () {
		//update the HUD
		hud.text = "Time: " + timer.ToString ();
	}

	void FixedUpdate(){

		if (clockRunning) {
			Debug.Log ("Clock is running");
			timer += Time.fixedDeltaTime;
		}
		else{ //clock is not running
			Debug.Log("Clock is not running");
		}
	}

	public static void timerController(bool startSet){
		clockRunning = startSet;
	}
}