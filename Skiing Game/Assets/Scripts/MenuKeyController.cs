﻿using UnityEngine;
using System.Collections;

public class KeyController : MonoBehaviour {

	//Manages keyboard events when in the main menu
	void LateUpdate () {

		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit();
		}
	}
}
