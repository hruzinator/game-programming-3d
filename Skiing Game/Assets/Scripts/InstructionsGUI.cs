﻿using UnityEngine;
using System.Collections;

public class InstructionsGUI : MonoBehaviour {

	public GameObject backGUI;

	private string text;
	private Vector2 screenCenter;
	private Vector2 instrDims = new Vector2(800f, 250f);


	void Start() {
		gameObject.SetActive (false);
		screenCenter = new Vector2 (Screen.width / 2, Screen.height / 2);
		text = "===Welcome to Ski Simulator!===\n\nThe goal of this game is to get from the top of the hill to the bottom of the hill the fastest while staying within the blue lines.\n\nDefault Controls:\n(Note these keybindings may be set differently in the launcher window)\n\nW - move forward\nS - move backward\nA - turn left\nD - turn right\nSpace - Jump\nF1 - toggle 1st and 3rd person cameras\nEscape - Go Back/Quit";
	}

	void OnGUI(){
		if(GUI.Button(new Rect(10, 10, 50, 30), "Back")) {
			gameObject.SetActive(false);
			backGUI.SetActive(true);
		}
		// Make a background box
		float xOffset = instrDims.x/2;
		float yOffset = instrDims.y/2;
		GUI.Box(new Rect(screenCenter.x-xOffset,screenCenter.y-yOffset, instrDims.x, instrDims.y), text);

	}
}
