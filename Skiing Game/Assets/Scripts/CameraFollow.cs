﻿using UnityEngine;
using System.Collections;

//TODO decide which tracking type you like best.
//TODO LERP!!!!!

public class CameraFollow : MonoBehaviour {

	public GameObject target;
	public Vector3 offset = new Vector3(0f, 3f, -2.47f);
	public Vector3 target_rot_offset = new Vector3(0f, 2f, 0f);
	
	// Update is called once per frame
	void LateUpdate () {
		float desiredAngle = target.transform.eulerAngles.y;
		Quaternion rotation = Quaternion.Euler(0, desiredAngle, 0);
		transform.position = target.transform.position + (rotation * offset);
		transform.LookAt (target.transform.position + target_rot_offset);
	}
}
