﻿using UnityEngine;
using System.Collections;

public class ski_move_test : MonoBehaviour {
	
	public GameObject startObj;

	public float forwardSpeed = 10f;
	public float sideSpeed = 10f;
	public float maxSpeed = 25f;
	public float turnSpeed = 100f;
	public float rotateBackDegs = 15f;
	public float jumpForce = 10000f;

	//private float lastJump = 0f;
	private Transform initPos;
	
	void Start(){
		transform.position = startObj.transform.position;
		transform.rotation = startObj.transform.rotation;
	}

	void Update(){
	}

	void FixedUpdate() {
		float v = Input.GetAxis ("Vertical");
		float h = Input.GetAxis ("Horizontal");

		if (v!=0 && rigidbody.velocity.magnitude < maxSpeed) {
			//rigidbody.AddRelativeTorque(Vector3.forward*turnSpeed * v);
			rigidbody.AddRelativeForce(Vector3.forward*forwardSpeed * v, ForceMode.Acceleration);
		}
		
		if (h != 0) {
			//TODO if rotation is less than boundries, then add torque
			rigidbody.AddRelativeTorque(new Vector3(0, 1, 0)*h*turnSpeed);
			//rigidbody.AddRelativeForce(Vector3.right*h*sideSpeed);
		}

		if(Input.GetKeyDown(KeyCode.Q)){
			rigidbody.AddRelativeTorque(Vector3.forward*turnSpeed);
		}
		if(Input.GetKeyDown(KeyCode.E)){
			rigidbody.AddRelativeTorque(-Vector3.forward*turnSpeed);
		}
	}
	
	void LateUpdate(){
		var rot = transform.rotation.eulerAngles;
		var rotVel = rigidbody.angularVelocity;
		if (rot.z >= 180f) {
			rot.z = Mathf.Clamp (rot.z, 270f, 359f);
		} else{
			rot.z = Mathf.Clamp (rot.z, 0f, 90f);
		}
		if (rot.x >= 180f) {
			rot.x = Mathf.Clamp(rot.x, 270f, 359f);
		}
		else{
			rot.x = Mathf.Clamp (rot.x, 0f, 90f);
		}

		if (rot.z < 270f || rot.z > 359f) {
			rigidbody.angularVelocity.Set (rotVel.x, rotVel.y, 0);
		}
		transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler (rot.x, rot.y, rot.z), 0.1f);
	}

	
}
