﻿using UnityEngine;
using System.Collections;

public class speedHUDget : MonoBehaviour {

	public GameObject refObj;
	
	void Update () {
		gameObject.GetComponent<GUIText> ().text = refObj.rigidbody.velocity.magnitude.ToString();
	}
}
