﻿using UnityEngine;
using System.Collections;

public class MainMenuButtons : MonoBehaviour {

	public Texture2D buttonTexture;
	public GameObject instructGUI;

	private Vector2 screenCenter;
	private Vector2 menuDims = new Vector2(300f, 170f);

	void Start(){
		screenCenter = new Vector2 (Screen.width / 2, Screen.height / 2);
	}

	void OnGUI () {
		// Make a background box
		float xOffset = menuDims.x/2;
		float yOffset = menuDims.y/2;
		GUI.Box(new Rect(screenCenter.x-xOffset,screenCenter.y-yOffset,menuDims.x,menuDims.y), "Loader Menu");
		
		// Make the first button. If it is pressed, Application.Loadlevel (1) will be executed
		if(GUI.Button(new Rect(screenCenter.x-xOffset,screenCenter.y-yOffset + 30,menuDims.x, 30), "Begin")) {
			Application.LoadLevel(1);
		}

		// Make a button for outlining game information
		if(GUI.Button(new Rect(screenCenter.x-xOffset,screenCenter.y-yOffset + 75,menuDims.x, 30), "Instructions")) {
			gameObject.SetActive(false);
			instructGUI.SetActive(true);
		}

		// Make an exit button
		if(GUI.Button(new Rect(screenCenter.x-xOffset,screenCenter.y-yOffset + 120,menuDims.x, 30), "Quit")) {
			Application.Quit();
		}
	}
}
