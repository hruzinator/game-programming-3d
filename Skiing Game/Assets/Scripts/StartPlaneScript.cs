﻿using UnityEngine;
using System.Collections;

public class StartPlaneScript : MonoBehaviour {

	public AudioSource soundtrack;

	void Start(){
		if(soundtrack.isPlaying)
			soundtrack.Stop ();
	}

	void OnTriggerEnter(Collider col){
		if (col.tag == "Player") {
			TKController.timerController (true);
			soundtrack.Play();
		}
	}
}
